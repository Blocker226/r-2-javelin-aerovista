﻿using UnityEngine;
using System.Collections;

public class FaceCamera : MonoBehaviour {

	Transform mainCam;

	// Use this for initialization
	void Start () {
		mainCam = GameObject.FindGameObjectWithTag("MainCamera").transform;
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(mainCam);
	}
}
