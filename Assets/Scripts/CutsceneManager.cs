﻿using UnityEngine;
using System.Collections;

public class CutsceneManager : MonoBehaviour {

	Camera mainCam;
	Camera cineCam;
	GameObject[] hotspots;
	// Use this for initialization
	void Start () {
		hotspots = GameObject.FindGameObjectsWithTag("Hotspot");

		mainCam = Camera.main;
		cineCam = GameObject.FindGameObjectWithTag("CinematicCamera").GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update() {
		if (Input.anyKeyDown && !Input.GetMouseButton(0) && cineCam.enabled) {
			SwitchBackToFPC();
		}
	}

	void SwitchBackToFPC () {
		mainCam.enabled = true;
		mainCam.GetComponent<AudioListener>().enabled = true;
		cineCam.enabled = false;
		cineCam.GetComponent<AudioListener>().enabled = false;
		cineCam.GetComponent<AudioSource>().Stop();
		foreach (GameObject hotspot in hotspots) {
			hotspot.SetActive(true);
		}
	}

	void OpenInfoBox (string infoBox) {
		GameObject boxObject = GameObject.Find(infoBox);
		Animator anim = boxObject.GetComponent<Animator>();
		anim.SetBool("isOpen", true);
	}

	void CloseInfoBox (string infoBox) {
		GameObject boxObject = GameObject.Find(infoBox);
		Animator anim = boxObject.GetComponent<Animator>();
		anim.SetBool("isOpen", false);
	}
}
