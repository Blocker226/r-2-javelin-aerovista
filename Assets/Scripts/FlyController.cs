﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof (CapsuleCollider))]
[RequireComponent(typeof (Rigidbody))]

public class FlyController : MonoBehaviour {


	public FirstPersonController fpcScript;
	public CharacterController charControl;
	public float flySpeed = 1;
	public float flyDrag = 2;
	public float jumpCatchTime = 1;

	AudioSource noiseMaker;
	CapsuleCollider airCollider;
	Rigidbody body;
	bool flying = false;
	int jumpCount = 0;

	// Use this for initialization
	void Start () {
		noiseMaker = GetComponent<AudioSource>();
		body = GetComponent<Rigidbody>();
		airCollider = GetComponent<CapsuleCollider>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Jump")) {
			if (jumpCount == 1 && jumpCatchTime > 0) {
				jumpCount = 0;
				ToggleFlight();
			}
			else {
				jumpCatchTime = 1;
				jumpCount++;
			}
		}
		if (jumpCatchTime > 0) {
			jumpCatchTime -= Time.deltaTime;
		}
		else {
			jumpCount = 0;
		}
	}

	void FixedUpdate() {
		if (flying) {
			if (Input.GetButton("Jump")) {
				body.AddForce(Vector3.up * flySpeed / 2);
			}
			if (Input.GetKey(KeyCode.LeftShift)) {
				body.AddForce(Vector3.down * flySpeed / 2);
			}
			if (Input.GetAxis("Horizontal") != 0.0f) {
				body.AddRelativeForce(Vector3.right * flySpeed * Input.GetAxis("Horizontal"));
			}
			if (Input.GetAxis("Vertical") != 0.0f) {
				body.AddRelativeForce(Vector3.forward * flySpeed * Input.GetAxis("Vertical"));
			}
		}
	}

	void ToggleFlight() {
		if (flying == false) {
			charControl.enabled = false;
			airCollider.enabled = true;
			body.useGravity = false;
			body.drag = flyDrag;
			body.isKinematic = false;
			noiseMaker.mute = true;
			flying = true;
		}
		else {
			fpcScript.enabled = true;
			charControl.enabled = true;
			airCollider.enabled = false;
			body.useGravity = true;
			body.drag = 0;
			body.isKinematic = true;
			noiseMaker.mute = false;
			flying = false;
		}
	}
}
